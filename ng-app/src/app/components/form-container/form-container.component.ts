import { Component, ComponentFactoryResolver, Inject, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { FORM_CONFIGURATION, IFormConfiguration } from 'src/app/services/form-configuration.interface';

@Component({
  selector: 'app-form-container',
  templateUrl: './form-container.component.html',
  styleUrls: ['./form-container.component.sass']
})
export class FormContainerComponent implements OnInit {
  @ViewChild("formContainer", { read: ViewContainerRef, static: true }) formContainer;
  componentRef: any;

  // new
  constructor(
    @Inject(FORM_CONFIGURATION) private formConfig : IFormConfiguration,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {     
  }

  // init
  ngOnInit() {    

    // check if a form configured
    if(this.formConfig.formComponent)
      this.setupForm(this.formConfig.formComponent);
  }

  ngOnDestroy() {    
    // clean up  
  }

  // sets up the form
  setupForm(componentType) {

    // clear the form, then add the form
    this.formContainer.clear();
    const factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
    this.componentRef = this.formContainer.createComponent(factory);
  }

}
