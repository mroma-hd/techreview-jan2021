import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-local-nav',
  templateUrl: './local-nav.component.html',
  styleUrls: ['./local-nav.component.sass']
})
export class LocalNavComponent implements OnInit {
  @Input() items : any[];
  
  constructor() { }

  ngOnInit(): void {
  }

}
