import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Area1PageComponent } from './area1-page.component';

describe('Area1PageComponent', () => {
  let component: Area1PageComponent;
  let fixture: ComponentFixture<Area1PageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Area1PageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Area1PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
