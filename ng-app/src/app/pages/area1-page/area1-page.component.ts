import { Component, OnInit } from '@angular/core';
import { FORM_CONFIGURATION } from 'src/app/services/form-configuration.interface';
import { Form1ConfigService } from 'src/app/services/form1-config.service';

@Component({
  selector: 'app-area1-page',
  templateUrl: './area1-page.component.html',
  styleUrls: ['./area1-page.component.sass'],
  providers: [
    { provide: FORM_CONFIGURATION, useClass: Form1ConfigService }, 
  ]
})
export class Area1PageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
