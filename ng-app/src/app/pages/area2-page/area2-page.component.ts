import { Component, OnInit } from '@angular/core';
import { FORM_CONFIGURATION } from 'src/app/services/form-configuration.interface';
import { Form2ConfigService } from 'src/app/services/form2-config.service';

@Component({
  selector: 'app-area2-page',
  templateUrl: './area2-page.component.html',
  styleUrls: ['./area2-page.component.sass'],
  providers: [
    { provide: FORM_CONFIGURATION, useClass: Form2ConfigService }, 
  ]
})
export class Area2PageComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
