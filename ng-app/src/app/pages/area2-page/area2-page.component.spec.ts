import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Area2PageComponent } from './area2-page.component';

describe('Area2PageComponent', () => {
  let component: Area2PageComponent;
  let fixture: ComponentFixture<Area2PageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Area2PageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Area2PageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
