import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './components/layout/layout.component';
import { Form1Component } from './components/form1/form1.component';
import { Form2Component } from './components/form2/form2.component';
import { FormContainerComponent } from './components/form-container/form-container.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { Area1PageComponent } from './pages/area1-page/area1-page.component';
import { Area2PageComponent } from './pages/area2-page/area2-page.component';
import { LocalNavComponent } from './components/local-nav/local-nav.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    Form1Component,
    Form2Component,
    FormContainerComponent,
    HomePageComponent,
    Area1PageComponent,
    Area2PageComponent,
    LocalNavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
