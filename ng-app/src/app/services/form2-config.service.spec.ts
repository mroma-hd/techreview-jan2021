import { TestBed } from '@angular/core/testing';

import { Form2ConfigService } from './form2-config.service';

describe('Form2ConfigService', () => {
  let service: Form2ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Form2ConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
