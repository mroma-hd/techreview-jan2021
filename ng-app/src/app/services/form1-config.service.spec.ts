import { TestBed } from '@angular/core/testing';

import { Form1ConfigService } from './form1-config.service';

describe('Form1ConfigService', () => {
  let service: Form1ConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(Form1ConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
