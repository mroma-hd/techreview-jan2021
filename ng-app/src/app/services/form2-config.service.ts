import { Injectable } from '@angular/core';
import { Form2Component } from '../components/form2/form2.component';
import { IFormConfiguration } from './form-configuration.interface';

@Injectable({
  providedIn: 'root'
})
export class Form2ConfigService implements IFormConfiguration {

  constructor() { }

  formComponent: any = Form2Component;
}
