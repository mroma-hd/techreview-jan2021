import { Injectable } from '@angular/core';
import { Form1Component } from '../components/form1/form1.component';
import { IFormConfiguration } from './form-configuration.interface';

@Injectable({
  providedIn: 'root'
})
export class Form1ConfigService implements IFormConfiguration {
  
  formComponent: any = Form1Component;
}
