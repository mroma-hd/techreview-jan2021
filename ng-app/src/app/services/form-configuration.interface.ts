// injection token
export const FORM_CONFIGURATION = "IFormConfiguration";

// form config
export interface IFormConfiguration {  

  // form
  formComponent : any;
}
