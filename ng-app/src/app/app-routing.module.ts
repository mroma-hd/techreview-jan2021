import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Area1PageComponent } from './pages/area1-page/area1-page.component';
import { Area2PageComponent } from './pages/area2-page/area2-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'area-1', component: Area1PageComponent },
  { path: 'area-2', component: Area2PageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
